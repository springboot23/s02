package com.b129.s02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S02Application {

	public static void main(String[] args) {
		SpringApplication.run(S02Application.class, args);
	}

}