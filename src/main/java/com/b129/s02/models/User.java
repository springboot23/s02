package com.b129.s02.models;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;

    @Entity
    @Table(name = "posts")
    public class User {

        // Properties

        @Id                         // primary key
        @GeneratedValue             // auto-incremented
        private Long id;

        @Column                     // field
        private String userName;

        @Column                     // field
        private String passWord;

        // Constructors
        public User() {}

        public User(String userName, String passWord) {
            this.userName = userName;
            this.passWord = passWord;
        }

        // Getters and Setters
        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getPassWord() {
            return passWord;
        }

        public void setPassWord(String passWord) {
            this.passWord = passWord;
        }
    }

